# Universal Subscriber

Load information about new videos on YouTube, Nebula, or Vimeo, and append records to an org-mode file.

Made with babashka.

## Usage

Commandline:

``` sh
bb --classpath src --main main -- config.edn
```

Cron (every four hours):

```
0 */4 * * * path/to/bb --classpath path/to/universal-subscriber/src --main main -- path/to/universal-subscriber/config.edn >> /tmp/universal_subscriber_logs.txt 2>&1
```

