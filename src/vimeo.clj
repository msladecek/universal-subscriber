(ns vimeo
  (:require
   [babashka.curl :as curl]
   [cheshire.core :as json]
   [common :refer [recent?]]))


(defn unpaged
  ([call-fn url params] (unpaged call-fn url params (constantly true)))
  ([call-fn url params page-while-fn]
   (loop [page 1
          results []]
     (let [response
           (call-fn url (-> params
                            (assoc-in [:query-params "page"] (str page))))
           data (-> response :body (json/parse-string true))
           results (into results (:data data))
           next-page (get-in data [:paging :next])]
       (if (and next-page (page-while-fn data))
         (recur (inc page) results)
         results)))))


(defn get-followed-users [{:keys [api-url api-token]}]
  (let [results (unpaged curl/get
                         (str api-url "/me/following")
                         {:query-params {"per_page" "100"}
                          :headers {"Authorization" (str "bearer " api-token)}})]
    (map #(select-keys % [:name :uri]) results)))


(defn get-user-videos [uri page-while-fn {:keys [api-url api-token]}]
  (unpaged curl/get
           (str api-url uri "/videos")
           {:query-params {"sort" "modified_time"
                           "direction" "desc"
                           "per_page" "100"}
            :headers {"Authorization" (str "bearer " api-token)}}
           page-while-fn))


(defn get-recent-videos
  ([config]
   (let [users (get-followed-users config)]
     (get-recent-videos users config)))
  ([users {:keys [recent-video-interval-hours] :as config}]
   (->> users
        (mapcat (fn [{user-uri :uri}]
                  (get-user-videos
                   user-uri
                   (fn [data]
                     (every?
                      (fn [timestamp]
                        (recent? (java.time.ZonedDateTime/parse timestamp) recent-video-interval-hours))
                      (map :modified_time data)))
                   config)))
        (map (fn [video]
               {:published (java.time.ZonedDateTime/parse (:release_time video))
                :title (:name video)
                :id (:uri video)
                :link (:link video)
                :creator (get-in video [:user :name])})))))
