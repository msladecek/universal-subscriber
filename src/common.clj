(ns common)


(defn recent?
  ([datetime] (recent? datetime 24))
  ([datetime hours]
   (.isAfter datetime
             (.minusHours (java.time.ZonedDateTime/now) hours))))
