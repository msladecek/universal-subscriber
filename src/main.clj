(ns main
  (:require
   [clojure.pprint :refer [pprint]]
   [clojure.set :as set]
   [clojure.edn :as edn]
   [youtube]
   [nebula]
   [vimeo]))


(defn video-data->id [video-data]
  [(:service video-data) (:id video-data)])


(defn video-data->notificaiton-str [{:keys [service creator title link]}]
  (format "* %s [ %s ]: =%s=\n%s\n\n" (.toUpperCase (name service)) creator title link))


(defn append-notifications! [filename video-data]
  (spit filename (apply str (map video-data->notificaiton-str video-data)) :append true))


(defn add-visited-ids! [filename video-data]
  (let [video-ids (set (map video-data->id video-data))
        file-content (edn/read-string (slurp filename))]
    (spit
     filename
     (with-out-str
       (pprint (update file-content :visited-ids set/union video-ids))))))


(defn try-report [label func & args]
  (println (str (java.time.OffsetDateTime/now)))
  (try
    (let [value (apply func args)]
      (println "Success" label "count:" (count value))
      value)
    (catch clojure.lang.ExceptionInfo e
      (println (str "Error " label "\n" e)))))


(defn -main [config-filename]
  (let [{{inbox-filename :inbox-file, db-filename :database-file} :main,
         youtube-config :youtube,
         nebula-config :nebula
         vimeo-config :vimeo}
        (-> config-filename slurp edn/read-string)

        recent-videos (concat
                       (->> (try-report "vimeo" vimeo/get-recent-videos vimeo-config)
                            (map #(assoc % :service :vimeo)))
                       (->> (try-report "youtube" youtube/get-recent-videos youtube-config)
                            (map #(assoc % :service :youtube)))
                       (->> (try-report "nebula" nebula/get-recent-videos nebula-config)
                            (map #(assoc % :service :nebula))))
        already-visited-ids (-> db-filename slurp edn/read-string (get :visited-ids #{}))
        recent-ids (set (map video-data->id recent-videos))
        to-notify (set/difference recent-ids already-visited-ids)
        notify-data (->> recent-videos
                         (filter #(to-notify (video-data->id %)))
                         (sort-by :published-at))]
    (append-notifications! inbox-filename notify-data)
    (add-visited-ids! db-filename notify-data)))
