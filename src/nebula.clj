(ns nebula
  (:require
   [babashka.curl :as curl]
   [cheshire.core :as json]
   [common :refer [recent?]]))


(defn unpaged
  ([call-fn url params] (unpaged call-fn url params (constantly true)))
  ([call-fn url params page-while-fn]
   (loop [page 1
          results []]
     (let [response
           (call-fn url (-> params
                            (assoc-in [:query-params "page"] (str page))))
           data (-> response :body (json/parse-string true))
           results (into results (:results data))
           next-page (:next data)]
       (if (and next-page (page-while-fn results))
         (recur (inc page) results)
         results)))))


(defn get-auth-header [{:keys [api-url auth-token]}]
  (let [response (curl/post (str api-url "/api/v1/authorization/")
                            {:headers {"Authorization" (str "Token " auth-token)}})
        data (-> response :body (json/parse-string true))]
    {"Authorization" (str "Bearer " (:token data))}))


(defn get-recent-videos [{:keys [recent-video-interval-hours content-api-url] :as config}]
  (let [auth-header (get-auth-header config)
        results (unpaged curl/get
                         (str content-api-url "/library/video")
                         {:headers auth-header}
                         (fn [data]
                           (every?
                            (fn [{:keys [published_at]}]
                              (recent? (java.time.ZonedDateTime/parse published_at) recent-video-interval-hours))
                            data)))]
    (->> results
         (map (fn [{:keys [title published_at slug channel_title share_url]}]
                {:published-at (java.time.ZonedDateTime/parse published_at)
                 :title title
                 :link share_url
                 :id slug
                 :creator channel_title}))
         (filter #(recent? (:published-at %) recent-video-interval-hours)))))
