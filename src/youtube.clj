(ns youtube
  (:require
   [babashka.curl :as curl]
   [cheshire.core :as json]
   [common :refer [recent?]]))


(defn unpaged
  ([call-fn url query-params] (unpaged call-fn url query-params (constantly true)))
  ([call-fn url query-params page-while-fn]
   (loop [page-token nil
          results []]
     (let [response
           (call-fn url {:query-params
                         (cond-> query-params
                           page-token (assoc "pageToken" page-token))})
           data (-> response :body (json/parse-string true))
           results (into results (:items data))
           next-page-token (:nextPageToken data)]
       (if (and next-page-token (page-while-fn data))
         (recur next-page-token results)
         results)))))


(defn get-subscriptions [{:keys [api-url api-key channel-id]}]
  (let [results (unpaged curl/get
                         (str api-url "/subscriptions")
                         {"part" "snippet,contentDetails"
                          "maxResults" "50"
                          "key" api-key
                          "channelId" channel-id})]
    (set (map #(get-in % [:snippet :resourceId :channelId]) results))))


(defn get-uploads-playlists [channel-ids {:keys [api-url api-key]}]
  (let [results
        (mapcat (fn [channel-ids-batch]
                  (let [response
                        (curl/get
                         (str api-url "/channels")
                         {:query-params {"part" "contentDetails"
                                         "id" (clojure.string/join "," channel-ids-batch)
                                         "key" api-key
                                         "maxResults" "50"}})]
                    (-> response :body (json/parse-string true) :items)))
                (partition-all 50 channel-ids))]
    (set (map #(get-in % [:contentDetails :relatedPlaylists :uploads]) results))))


(defn get-playlist-videos [playlist-id page-while-fn {:keys [api-url api-key]}]
  (try
    (-> (unpaged curl/get
                 (str api-url "/playlistItems")
                 {"playlistId" playlist-id
                  "part" "snippet"
                  "key" api-key}
                 page-while-fn))
    (catch clojure.lang.ExceptionInfo e
      (clojure.pprint/pprint (ex-data e)))))


(defn get-recent-videos
  ([config]
   (let [subscriptions-channel-ids (get-subscriptions config)
         uploads-playlists-ids (get-uploads-playlists subscriptions-channel-ids config)]
     (get-recent-videos uploads-playlists-ids config)))
  ([playlist-ids {:keys [recent-video-interval-hours video-link-prefix] :as config}]
   (let [results
         (mapcat (fn [playlist-id]
                   (get-playlist-videos
                    playlist-id
                    (fn [data]
                      (every?
                       (fn [published-at]
                         (recent? (java.time.ZonedDateTime/parse published-at) recent-video-interval-hours))
                       (map #(get-in % [:snippet :publishedAt]) (:items data))))
                    config))
                 playlist-ids)]
     (->> results
          (map (fn [data]
                 (let [video-id (get-in data [:snippet :resourceId :videoId])]
                   {:id video-id
                    :published-at (java.time.ZonedDateTime/parse (get-in data [:snippet :publishedAt]))
                    :creator (get-in data [:snippet :channelTitle])
                    :title (get-in data [:snippet :title])
                    :link (str video-link-prefix video-id)})))
          (filter #(recent? (:published-at %) recent-video-interval-hours))))))
